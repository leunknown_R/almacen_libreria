﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using EntityLayer;

namespace PresentationLayer.Windows
{
    /// <summary>
    /// Lógica de interacción para WinAddUpdateProduct.xaml
    /// </summary>
    public partial class WinAddUpdateProduct : Window
    {
        #region Atributos
        private TypeWindow typeWindow;
        private Product product;
        #endregion
        public WinAddUpdateProduct()
        {
            InitializeComponent();
            typeWindow = TypeWindow.Add;
            product = new Product();
        }
        public WinAddUpdateProduct(Product product)
        {
            InitializeComponent();
            typeWindow = TypeWindow.Update;
            this.product = product;
            // Cambiando título
            Title = "Actualizar Producto";
            btnAction.Content = "Actualizar";
        }
        private void ExecuteAction(object sender, RoutedEventArgs evt)
        {
            actions[typeWindow]();
        }
        private Dictionary<TypeWindow, Action> actions = new Dictionary<TypeWindow, Action>
        {
            {
                TypeWindow.Add, () =>
                {
                    MessageBox.Show("Agregar");
                }
            }, 
            {
                TypeWindow.Update, () =>
                {
                    MessageBox.Show("Actualizar");
                }
            }
        };
    }
}
