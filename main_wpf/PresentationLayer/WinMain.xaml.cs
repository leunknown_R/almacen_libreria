﻿using System.Collections.Generic;
using System.Windows;
using System.Data;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using DomainLayer;
using EntityLayer;
using PresentationLayer.Windows;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class WinMain : Window
    {
        private Product product;
        public WinMain()
        {
            InitializeComponent();
            LoadProductOnDataGrid();
            product = new Product();
        }
        #region Métodos
        #region Privados
        private bool LoadProductOnDataGrid()
        {
            List<Product> products = DProduct.GetProducts();
            if (products is null)
            {
                MessageBox.Show("No hay registros en la tabla.", "Información", 
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            datagProducts.ItemsSource = products;
            return true;
        }
        private void RefreshProducts(object sender, RoutedEventArgs evt)
        {
            if (LoadProductOnDataGrid()) 
                MessageBox.Show("¡Refrescado Exitosamente!");
        }
        private void ShowWinAddProduct(object sender, RoutedEventArgs evt)
        {
            new WinAddUpdateProduct().ShowDialog();
        }
        private void ShowWinUpdateProduct(object sender, RoutedEventArgs evt)
        {
            new WinAddUpdateProduct(
                (Product)((Button)evt.OriginalSource).DataContext).ShowDialog();
        }
        private void DeleteProduct(object sender, RoutedEventArgs evt)
        {
            MessageBoxResult mbResult = MessageBox.Show(
                "¿Está seguro que desea eliminar este registro?", 
                "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Question);
            // Decidiendo si se presionó "No"
            if (mbResult.Equals(MessageBoxResult.No))
                return;
            // Intentando eliminar el producto a través de la DomainLayer
            List<Product> newProducts = DProduct.DeleteProduct(
                (Product) ((Button) evt.OriginalSource).DataContext, 
                (List<Product>) datagProducts.ItemsSource);
            // Definiendo función interna para mostrar el mensaje final
            System.Action<string, string, MessageBoxImage> showFinalMsg = 
                (msg, title, mbImg) =>
                MessageBox.Show(msg, title, MessageBoxButton.OK, mbImg);
            // Validando acción a realizar
            if (newProducts is null)
            {
                showFinalMsg("Ocurrió un error inesperado...", "Error", 
                    MessageBoxImage.Error);
                return;
            }
            datagProducts.ItemsSource = newProducts;
            showFinalMsg("¡Eliminado con éxito!", "Información", 
                MessageBoxImage.Information);
        }
        #endregion
        #endregion
    }
}
