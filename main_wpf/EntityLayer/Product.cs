﻿using System;

namespace EntityLayer
{
    public class Product
    {
        #region Atributos
        public uint Id { get; set; }
        public string NameProduct { get; set; }
        public uint Stock { get; set; } 
        public double Price { get; set; }
        public uint IdBrand { get; set; }
        public string Brand { get; set; }
        #endregion
        #region Constructores
        public Product() 
        {
            Id = 0;
            NameProduct = "";
            Stock = 0;
            Price = 0;
            IdBrand = 0;
            Brand = "";
        }
        public Product(uint Id, string NameProduct, uint Stock, double Price, uint IdBrand, string Brand)
        {
            this.Id = Id;
            this.NameProduct = NameProduct;
            this.Stock = Stock;
            this.Price = Price;
            this.IdBrand = IdBrand;
            this.Brand = Brand;
        }
        #endregion
    }
}
