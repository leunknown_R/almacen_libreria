﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Tools
{
    public class ConvertToListEntity<E>
    {
        public static List<E> FromDataTable(Action<List<E>, DataRow> AddEntity, DataTable data)
        {
            List<E> entityList = new List<E>();
            foreach (DataRow dataRow in data.Rows)
            {
                AddEntity(entityList, dataRow);
            }
            return entityList;
        }
    }
}
