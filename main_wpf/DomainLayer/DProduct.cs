﻿using System;
using System.Collections.Generic;
using System.Data;
using DataLayer.Actions;
using EntityLayer;
using DomainLayer.Tools;

namespace DomainLayer
{
    public class DProduct
    {
        #region Métodos
        #region Públicos
        public static List<Product> GetProducts()
        {
            DataTable data = GetDB.GetProducts();
            return data is null 
                ? null : ConvertToList(data);
        }
        public static List<Product> AddProduct(Product product)
        {
            DataTable data = InsertDB.AddProduct(product);
            return data is null 
                ? null : ConvertToList(data);
        }
        public static List<Product> DeleteProduct(
            Product productTarget, 
            List<Product> currentProducts)
        {
            bool success = DeleteDB.DeleteProduct(productTarget);
            return success
            ? currentProducts.FindAll(copyProd => copyProd.Id != productTarget.Id)
            : null;
        }
        #endregion
        #region Privados
        private static List<Product> ConvertToList(DataTable data)
        {
            return ConvertToListEntity<Product>.FromDataTable(
                (products, dataRow) =>
                {
                    products.Add(new Product
                    {
                        Id = uint.Parse(dataRow["ID_PRODUCT"] + ""),
                        NameProduct = dataRow["PRODUCT"] + "",
                        Stock = uint.Parse(dataRow["STOCK"] + ""),
                        Price = double.Parse(dataRow["PRICE"] + ""),
                        IdBrand = uint.Parse(dataRow["ID_BRAND"] + ""),
                        Brand = dataRow["BRAND"] + ""
                    });
                }, data);
        }
        #endregion
        #endregion
    }
}