﻿using System;
using System.Data;
using System.Data.SqlClient;
using DataLayer.Tools;
using EntityLayer;

namespace DataLayer.Actions
{
    public class DeleteDB
    {
        public static bool DeleteProduct(Product product)
        {
            SqlParameter[] parameters = 
            { 
                new SqlParameter("@id_product", (int) product.Id) 
            };
            bool success = QueryDB.ExecuteWithParameters(
                ProceduresDB.DELETE_PRODUCT, parameters
            );
            return success;
        }
    }
}
