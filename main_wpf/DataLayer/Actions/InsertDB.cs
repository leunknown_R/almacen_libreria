﻿using System;
using System.Data;
using System.Data.SqlClient;
using DataLayer.Tools;
using EntityLayer;

namespace DataLayer.Actions
{
    public class InsertDB
    {
        public static DataTable AddProduct(Product product)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@name_product", product.NameProduct),
                new SqlParameter("@stock", product.Stock),
                new SqlParameter("@price", product.Price),
                new SqlParameter("@id_brand", product.IdBrand)
            };
            bool success = QueryDB.ExecuteWithParameters(
                ProceduresDB.ADD_PRODUCT, parameters);
            return success
                ? QueryDB.Execute(ProceduresDB.GET_PRODUCTS)
                : null;
        }
    }
}
