﻿using System;
using System.Data;
using DataLayer.Tools;
using EntityLayer;

namespace DataLayer.Actions
{
    public class GetDB
    {
        public static DataTable GetProducts()
        {
            DataTable dataProducts = QueryDB.Execute(ProceduresDB.GET_PRODUCTS);
            return dataProducts;
        }
    }
}
