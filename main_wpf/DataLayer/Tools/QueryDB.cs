﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Tools
{
    internal class QueryDB
    {
        public static DataTable Execute(string procedure)
        {
            ConnectionDB connectionDB = new ConnectionDB();
            DataTable data = new DataTable();
            try
            {
                using (SqlCommand sqlCmd = new SqlCommand(
                    procedure,
                    connectionDB.ConnectDisconnect()))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sqlDAdap = new SqlDataAdapter(sqlCmd);
                    sqlDAdap.Fill(data);
                    return data;
                }
            }
            catch (Exception)
            {
                return null;
            }
            finally { connectionDB.ConnectDisconnect(); }
        }
        public static bool ExecuteWithParameters(
            string procedure, 
            SqlParameter[] parameters) 
        {
            ConnectionDB connectionDB = new ConnectionDB();
            try
            {
                using (SqlCommand sqlCmd = new SqlCommand(
                    procedure, connectionDB.ConnectDisconnect()))
                {
                    DataTable data = new DataTable();
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddRange(parameters);
                    SqlDataAdapter sqlDAdap = new SqlDataAdapter(sqlCmd);
                    sqlDAdap.Fill(data);
                }
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }
    }
}
