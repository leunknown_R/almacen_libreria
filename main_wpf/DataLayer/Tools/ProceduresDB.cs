﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public static class ProceduresDB
    {
        public const string GET_PRODUCTS = "inventory.sp_get_products";
        public const string ADD_PRODUCT = "inventory.sp_add_product";
        public const string UPDATE_PRODUCT = "inventory.sp_update_product";
        public const string DELETE_PRODUCT = "inventory.sp_delete_product";
    }
}
