﻿using System.Data;
using System.Data.SqlClient;

namespace DataLayer.Tools
{
    public class ConnectionDB
    {
        #region Atributos
        private SqlConnection connection;
        private const string STRING_CONNECT_DB = "Data Source=DESKTOP-KP9L0AD;Initial Catalog=db_crud_inventory_bookshop;Integrated Security=True";
        #endregion
        public ConnectionDB()
        {
            connection = new SqlConnection(STRING_CONNECT_DB);
        }
        #region Métodos
        public SqlConnection ConnectDisconnect()
        {
            if (connection.State.Equals(ConnectionState.Open))
            {
                connection.Close();
                return null;
            }
            connection.Open();
            return connection;
        }
        #endregion
    }
}
