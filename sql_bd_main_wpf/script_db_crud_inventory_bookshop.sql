/* BACKUP - CRUD INVENTORY BOOKSHOP DB */

USE master
GO
-- CREANDO BASE DE DATOS
DROP DATABASE IF EXISTS db_crud_inventory_bookshop
GO
CREATE DATABASE db_crud_inventory_bookshop
GO

USE db_crud_inventory_bookshop
GO


-- CREANDO ESQUEMAS
DROP SCHEMA IF EXISTS inventory
GO
CREATE SCHEMA inventory
GO


-- CREANDO TABLAS
DROP TABLE IF EXISTS inventory.brands
GO
CREATE TABLE inventory.brands(
	id_brand INT IDENTITY(1,1),
	name_brand VARCHAR(30), 
	available BIT, 
	CONSTRAINT pk_id_brand PRIMARY KEY (id_brand)
);

DROP TABLE IF EXISTS inventory.products
GO
CREATE TABLE inventory.products(
	id_product INT IDENTITY(1,1), 
	name_product VARCHAR(50) NOT NULL, 
	price DECIMAL(4,2) NOT NULL, 
	stock INT NOT NULL, 
	id_brand INT NOT NULL, 
	available BIT,
	CONSTRAINT ch_price_greater_than_0 CHECK (price > 0), 
	CONSTRAINT ch_stock_greater_than_or_equals_to_0 CHECK (stock >= 0), 
	CONSTRAINT pk_id_product PRIMARY KEY (id_product), 
	CONSTRAINT fk_id_brand FOREIGN KEY (id_brand) REFERENCES inventory.brands(id_brand)
);


-- INSERTANDO DATOS INICIALES
INSERT INTO inventory.brands(name_brand) VALUES 
	('Artesco'), 
	('Faber - Castell'), 
	('Paper Mate'), 
	('Ofiscool'), 
	('Justus');

INSERT INTO inventory.products(name_product, price, stock, id_brand, available) 
VALUES
	('Archivador Oficio Lomo Ancho Negro', 30, 8.20, 1, 1),
	('Archivador A4 Lomo Ancho Azul', 15, 6.50, 1, 1),
	('Archivador A4 Lomo Ancho Rojo', 11, 6.50, 1, 1),
	('Archivador A4 Lomo Ancho Negro', 9, 6.50, 1, 1),
	('Lápiz 2B', 50, 1.50, 2, 1),
	('Lápiz B', 43, 1.60, 2, 1),
	('Lápiz F', 20, 1.60, 2, 1),
	('Lápiz Mongol 2B', 40, 1.50, 3, 1),
	('Lápiz 2B Jumbo', 15, 2.20, 1, 1),
	('Ecolápices de Colores X60', 15, 45, 2, 1),
	('Ecolápices de Colores X12', 28, 11.90, 2, 1),
	('Papel Fotocopia A4', 23, 15, 5, 1),
	('Cuaderno A4 Cuadriculado X84 Hojas', 25, 3.20, 5, 1),
	('Cuaderno A4 Rayado X84 Hojas', 18, 3.20, 5, 1),
	('Cuaderno A4 Cuadriculado X72 Hojas', 34, 2.40, 4, 1),
	('Cuaderno A4 Rayado X72 Hojas', 29, 2.40, 4, 1),
	('Tajador Con Depósito', 29, 1.90, 1, 1),
	('Tajador Con Depósito', 35, 1.00, 2, 1),
	('Borrador Negro', 25, 1.50, 2, 1),
	('Borrador Mixto', 40, 0.70, 2, 1),
	('Borrador Blanco', 34, 1.20, 1, 1);


-- PROCEDIMIENTOS ALMACENADOS
-- OBTENER PRODUCTOS
DROP PROCEDURE IF EXISTS inventory.sp_get_products
GO
CREATE PROCEDURE inventory.sp_get_products
AS
BEGIN
	SELECT
		prod.id_product AS 'ID_PRODUCT',
		prod.name_product AS 'PRODUCT', 
		prod.stock AS 'STOCK', 
		prod.price AS 'PRICE', 
		br.id_brand AS 'ID_BRAND' ,
		br.name_brand AS 'BRAND' 
	FROM inventory.products prod INNER JOIN inventory.brands br 
	ON prod.id_brand = br.id_brand 
	WHERE prod.available = 1;
END
GO

-- AGREGAR PRODUCTO
DROP PROCEDURE IF EXISTS inventory.sp_add_product
GO
CREATE PROC inventory.sp_add_product
	@name_product VARCHAR (50),
	@stock INT,
	@price DECIMAL(4,2),
	@id_brand INT
AS
BEGIN
	INSERT INTO inventory.products VALUES (@name_product, @stock,  @price, @id_brand, 1);
	SELECT 'SUCCESS' AS 'RESPONSE';
END
GO

-- ACTUALIZAR PRODUCTO
DROP PROCEDURE IF EXISTS inventory.sp_update_product
GO
CREATE PROCEDURE inventory.sp_update_product
	@id_product INT, 
	@name_product VARCHAR(50), 
	@stock INT, 
	@price DECIMAL(4,2), 
	@id_brand INT 
AS
BEGIN
	IF EXISTS 
		(SELECT name_product FROM inventory.products WHERE id_product = @id_product) 
	BEGIN
		UPDATE 
			inventory.products 
		SET name_product = @name_product, 
			stock = @stock, 
			price = @price, 
			id_brand = @id_brand 
		WHERE id_product = @id_product;
		SELECT 'SUCCESS' AS 'RESPONSE';
	END
	ELSE SELECT 'FAILED' AS 'RESPONSE';
END
GO

-- ELIMINACIÓN DE PRODUCTO
DROP PROCEDURE IF EXISTS inventory.sp_delete_product
GO
CREATE PROCEDURE inventory.sp_delete_product
	@id_product INT
AS
BEGIN
	IF EXISTS 
		(SELECT name_product FROM inventory.products WHERE id_product = @id_product) 
	BEGIN 
		UPDATE inventory.products
		SET available = 0 
		WHERE id_product = @id_product;
		SELECT 'SUCCESS' AS 'RESPONSE';
	END
	ELSE SELECT 'FAILED' AS 'RESPONSE';
END
GO

-- OBTENER MARCAS
DROP PROCEDURE IF EXISTS inventory.sp_get_brands
GO
CREATE PROCEDURE inventory.sp_get_brands
AS
BEGIN
	SELECT 
		id_brand AS 'ID', 
		name_brand AS 'BRAND'
	FROM inventory.brands
	WHERE available = 1;
END
GO

-- PROCEDIMIENTOS
/*
EXECUTE inventory.sp_get_products;
EXECUTE inventory.sp_add_product 'Borrador pinga 3m', 15, 6.90, 2;
EXECUTE inventory.sp_update_product 22, 'Borrador pinga 4m', 17, 1, 1;
EXECUTE inventory.sp_delete_product 22;
*/

EXECUTE inventory.sp_get_products;
EXECUTE inventory.sp_delete_product 3;
EXECUTE inventory.sp_get_products;
UPDATE inventory.products SET available = 1;
EXECUTE inventory.sp_get_products;
