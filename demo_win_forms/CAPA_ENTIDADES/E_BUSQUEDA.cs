﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAPA_ENTIDADES
{
    public class E_BUSQUEDA
    {
        public string Criterio { get; set; }
        public string CodCriterio { get; set; }
        public E_BUSQUEDA()
        { }
        public E_BUSQUEDA(string codCriterio, string criterio)
        {
            CodCriterio = codCriterio;
            Criterio = criterio;
        }
    }
}