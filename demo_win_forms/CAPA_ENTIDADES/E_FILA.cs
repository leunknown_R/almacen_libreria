﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAPA_ENTIDADES
{
    public class E_FILA
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Stock { get; set; }
        public string Precio { get; set; }
        public string Marca { get; set; }
    }
}
