﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAPA_DATOS
{
    class D_PROCEDIMIENTOS
    {
        public const string CONSULTAR_TABLA_PRODUCTOS = "inventario.sp_consulta_tabla";
        public const string CONSULTAR_BUSQUEDA = "inventario.sp_consulta_busqueda";
        public const string INSERTAR_REGISTRO = "inventario.sp_insercion_registro";
        public const string ELIMINAR_REGISTRO = "inventario.sp_eliminacion_registro";
        public const string ACTUALIZAR_REGISTRO = "inventario.sp_actualizacion_registro";
    }
}
