﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CAPA_DATOS
{
    public class D_CONEXION
    {
        public SqlConnection LeerCadena()
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["BD"].ConnectionString);
            if (cn.State.Equals(ConnectionState.Open))
                cn.Close();
            else
                cn.Open();
            return cn;
        }
    }
}
