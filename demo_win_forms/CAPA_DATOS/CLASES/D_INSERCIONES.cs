﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CAPA_ENTIDADES;

namespace CAPA_DATOS
{
    public class D_INSERCIONES
    {
        D_CONEXION cn = new D_CONEXION();
        public string D_INSERTAR_REGISTRO(E_FILA e_fila)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand(D_PROCEDIMIENTOS.INSERTAR_REGISTRO, cn.LeerCadena()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@NOMBRE", e_fila.Nombre);
                    cmd.Parameters.AddWithValue("@STOCK", e_fila.Stock);
                    cmd.Parameters.AddWithValue("@PRECIO", e_fila.Precio);
                    cmd.Parameters.AddWithValue("@MARCA", e_fila.Marca);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    return dt.Rows[0]["RPTA"].ToString();
                }
            }
            catch (Exception)
            {
                return "ERROREXC";
            }
            finally
            {
                cn.LeerCadena();
            }
        }
    }
}
