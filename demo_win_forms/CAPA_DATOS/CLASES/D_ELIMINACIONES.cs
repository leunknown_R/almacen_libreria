﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CAPA_ENTIDADES;

namespace CAPA_DATOS.CLASES
{
    public class D_ELIMINACIONES
    {
        D_CONEXION cn = new D_CONEXION(); 
        public string D_ELIMINACION_REGISTRO(E_BUSQUEDA e_busqueda)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand(D_PROCEDIMIENTOS.ELIMINAR_REGISTRO, cn.LeerCadena()))
                {
                    DataTable dt = new DataTable();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CRITERIO", e_busqueda.Criterio);
                    cmd.Parameters.AddWithValue("@CODIGO_C", e_busqueda.CodCriterio);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    D_CONSULTAS.msm = dt.Rows[0]["RPTA"].ToString(); 
                    return dt.Rows[0]["RPTA"].ToString();
                }
            }
            catch (Exception ex)
            {
                D_CONSULTAS.msm = ex.Message;
                return "ERROREXC";
            }
            finally
            {
                cn.LeerCadena();
            }
        }
    }
}
