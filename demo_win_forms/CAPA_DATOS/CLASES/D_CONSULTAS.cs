﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CAPA_ENTIDADES;

namespace CAPA_DATOS
{
    public class D_CONSULTAS
    {
        D_CONEXION cn = new D_CONEXION();
        public static string msm;
        public DataTable D_TABLA_PRODUCTOS()
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand(D_PROCEDIMIENTOS.CONSULTAR_TABLA_PRODUCTOS, cn.LeerCadena()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    return dt;
                }
            }
            catch (Exception)
            {
                return new DataTable();
            }
            finally
            {
                cn.LeerCadena();
            }
        }
        public string D_INDICE_BUSQUEDA(E_BUSQUEDA e_busqueda)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand(D_PROCEDIMIENTOS.CONSULTAR_BUSQUEDA, cn.LeerCadena()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CRITERIO", e_busqueda.Criterio);
                    cmd.Parameters.AddWithValue("@CODIGO_C", e_busqueda.CodCriterio);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    msm = dt.Rows[0]["RPTA"].ToString() + " ESTOY EN TRY";
                    return dt.Rows[0]["RPTA"].ToString();
                }
            }
            catch (Exception e)
            {
                msm = e.Message;
                return "ERROR";
            }
            finally
            {
                cn.LeerCadena();
            }
        }
    }
}
