﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CAPA_ENTIDADES;

namespace CAPA_DATOS.CLASES
{
    public class D_ACTUALIZACIONES
    {
        D_CONEXION cn = new D_CONEXION();
        public string D_ACTUALIZACION_REGISTRO(E_FILA e_fila)
        {
            DataTable dt = new DataTable();
            try
            {                 
                using (SqlCommand cmd = new SqlCommand(D_PROCEDIMIENTOS.ACTUALIZAR_REGISTRO, cn.LeerCadena()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CODIGO", e_fila.Codigo);
                    cmd.Parameters.AddWithValue("@NOMBRE", e_fila.Nombre);
                    cmd.Parameters.AddWithValue("@STOCK", int.Parse(e_fila.Stock));
                    cmd.Parameters.AddWithValue("@PRECIO", double.Parse(e_fila.Precio));
                    cmd.Parameters.AddWithValue("@MARCA", e_fila.Marca);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    return dt.Rows[0]["RPTA"].ToString();
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
            finally
            {
                cn.LeerCadena();
            }
        }
    }
}
