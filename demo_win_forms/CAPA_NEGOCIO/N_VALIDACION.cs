﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAPA_ENTIDADES;

namespace CAPA_NEGOCIO
{
    class N_VALIDACION
    {
        public static bool ValidarE_fila(E_FILA e_fila)
        {
            //ELIMINANDO ESPACIOS DEL FINAL Y EL PRINCIPIO
            e_fila.Nombre = e_fila.Nombre.TrimEnd();
            e_fila.Nombre = e_fila.Nombre.TrimStart();
            e_fila.Marca = e_fila.Marca.TrimEnd();
            e_fila.Marca = e_fila.Marca.TrimStart();
            //VALIDANDO
            if (double.TryParse(e_fila.Precio, out double nDecimal))
                return true;
            else
                return false;
        }
        public static bool ValidarE_filaConCodigo(E_FILA e_fila)
        {
            //ELIMINANDO ESPACIOS DEL FINAL Y EL PRINCIPIO
            e_fila.Nombre = e_fila.Nombre.TrimEnd();
            e_fila.Nombre = e_fila.Nombre.TrimStart();
            e_fila.Marca = e_fila.Marca.TrimEnd();
            e_fila.Marca = e_fila.Marca.TrimStart();
            //VALIDANDO
            if (double.TryParse(e_fila.Precio, out double ndouble) &&
                ushort.TryParse(e_fila.Codigo, out ushort nushort))
                return true;
            else
                return false;
        }

        public static bool ValidarE_busqueda(E_BUSQUEDA e_busqueda)
        {
            if (e_busqueda.CodCriterio == "1" && int.TryParse(e_busqueda.Criterio, out int entero)) return true;
            else if (e_busqueda.CodCriterio == "0" && e_busqueda.Criterio != "=" && e_busqueda.Criterio != "[") return true;
            else return false;
        }
    }
}
