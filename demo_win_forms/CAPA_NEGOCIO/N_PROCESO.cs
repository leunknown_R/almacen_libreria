﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using CAPA_DATOS;
using CAPA_DATOS.CLASES;
using CAPA_ENTIDADES;

namespace CAPA_NEGOCIO
{
    public class N_PROCESO
    {
        D_CONSULTAS OBJ_CONSULTAS;
        D_INSERCIONES OBJ_INSERCIONES;
        D_ACTUALIZACIONES OBJ_ACTUALIZACIONES;
        D_ELIMINACIONES OBJ_ELIMINACIONES;
        public N_PROCESO()
        {
            OBJ_CONSULTAS = new D_CONSULTAS();
            OBJ_INSERCIONES = new D_INSERCIONES();
            OBJ_ACTUALIZACIONES = new D_ACTUALIZACIONES();
            OBJ_ELIMINACIONES = new D_ELIMINACIONES();
        }
        public DataTable OBTENER_TABLA_PRODUCTOS()
        {
            return OBJ_CONSULTAS.D_TABLA_PRODUCTOS();
        }
        public string OBTENER_INDICE_BUSQUEDA(E_BUSQUEDA e_busqueda)
        {
            string indice = "";

            if (N_VALIDACION.ValidarE_busqueda(e_busqueda)) indice = OBJ_CONSULTAS.D_INDICE_BUSQUEDA(e_busqueda); 
            else indice = "ERRORFMTO";
            return indice;
        }
        public string INSERTAR_REGISTRO(E_FILA e_fila)
        {
            string rpta = "";
            //VALIDANDO
            if (N_VALIDACION.ValidarE_fila(e_fila))
                rpta = OBJ_INSERCIONES.D_INSERTAR_REGISTRO(e_fila);
            else
                rpta = "ERRORFMTO";
            return rpta;
        }
        public string ACTUALIZAR_REGISTRO(E_FILA e_fila)
        {
            string rpta = "";
            //VALIDANDO
            if (N_VALIDACION.ValidarE_filaConCodigo(e_fila))
                rpta = OBJ_ACTUALIZACIONES.D_ACTUALIZACION_REGISTRO(e_fila);
            else
                rpta = "ERRORFMTO";
            return rpta;
        }
        public string ELIMINAR_REGISTRO(E_BUSQUEDA e_busqueda)
        {
            string rpta = OBJ_ELIMINACIONES.D_ELIMINACION_REGISTRO(e_busqueda);
            return rpta;
        }
    }
}
