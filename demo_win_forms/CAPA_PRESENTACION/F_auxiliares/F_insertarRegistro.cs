﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CAPA_ENTIDADES;
using CAPA_NEGOCIO;

namespace CAPA_PRESENTACION.F_auxiliares
{
    public partial class F_insertarRegistro : Form
    {
        private E_FILA e_fila;
        private N_PROCESO n_proceso;
        private bool fueInsertado;

        public F_insertarRegistro()
        {
            InitializeComponent();
            InicializarValores();
        }
        private void InicializarValores()
        {
            //INICIALIZAR VALORES
            e_fila = new E_FILA();
            n_proceso = new N_PROCESO();
            fueInsertado = false;
            //PROPIEDADES
            btn_insertar.Enabled = false;
        }
        #region MÉTODOS / FUNCIONES
        private string obtenerRPTA()
        {
            return n_proceso.INSERTAR_REGISTRO(e_fila);
        }
        private void procesarRPTA(string rpta)
        {
            switch (rpta)
            {
                case "ERRORFMTO":
                    MessageBox.Show("Uno de los formatos de los campo no es válido.", "Error", 
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_precio.Clear();
                    break;
                case "ERROREXC":
                    MessageBox.Show("Ocurrió un error inesperado...", "Error", 
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case "HECHO":
                    fueInsertado = true;
                    LimpiarEntradas();
                    MessageBox.Show("¡Registro insertado con éxito!", "Mensaje", 
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }
        public bool ObtenerFueInsertado()
        {
            return fueInsertado;
        }
        public void ReiniciarFueInsertado()
        {
            fueInsertado = false;
        }
        public void LimpiarEntradas()
        {
            txt_nombre.Clear();
            nud_stock.Value = 1;
            txt_precio.Clear();
            txt_marca.Clear();
        }
        #endregion
        #region EVENTOS
        private void btn_insertar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿ESTÁ SEGURO DE LOS VALORES QUE DESEA INSERTAR?", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.Yes)
            {
                //RECIBIENDO VALORES
                string nombre = txt_nombre.Text, stock = nud_stock.Value.ToString(), precio = txt_precio.Text, marca = txt_marca.Text;
                e_fila.Nombre = nombre;
                e_fila.Stock = stock;
                e_fila.Precio = precio;
                e_fila.Marca = marca;
                string rpta = obtenerRPTA();
                procesarRPTA(rpta);
                fueInsertado = true;
            }
        }
        private void EventoTextChanged_entradaDatos(object sender, EventArgs e)
        {
            if (String.Empty == txt_nombre.Text || 
                String.Empty == txt_precio.Text || 
                String.Empty == txt_marca.Text || 
                nud_stock.Value == 1) btn_insertar.Enabled = false;
            else btn_insertar.Enabled = true;
        }
        private void EventoKeyPress_entradaDatos(object sender, KeyPressEventArgs e)
        {
            if (txt_nombre.Focused || txt_marca.Focused)
            {
                if (Char.IsNumber(e.KeyChar) ||
                    Char.IsLetter(e.KeyChar) ||
                    e.KeyChar == '-' ||
                    e.KeyChar == Convert.ToChar(Keys.Back) ||
                    e.KeyChar == Convert.ToChar(Keys.Space)) e.Handled = false;
                else e.Handled = true;
            }
            else if (txt_precio.Focused)
            {
                if (Char.IsNumber(e.KeyChar) || 
                    e.KeyChar == '.' ||
                    e.KeyChar == Convert.ToChar(Keys.Back)) e.Handled = false;
                else e.Handled = true;
            }

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txt_nombre.Focused) nud_stock.Focus();
                else if (nud_stock.Focused) txt_precio.Focus();
                else if (txt_precio.Focused) txt_marca.Focus();
                else if (txt_marca.Focused)
                {
                    btn_insertar.Focus();
                    btn_insertar.PerformClick();
                }
            }
        }
        #endregion
    }
}
