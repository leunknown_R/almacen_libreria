﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CAPA_ENTIDADES;
using CAPA_NEGOCIO;
using CAPA_DATOS;

namespace CAPA_PRESENTACION.F_auxiliares
{
    public partial class F_actualizarRegistro : Form
    {
        private E_FILA e_fila;
        private N_PROCESO n_proceso;
        private bool fueActualizado;

        public F_actualizarRegistro(E_FILA e_fila)
        {
            InitializeComponent();
            InicializarValores(e_fila);
        }
        private void InicializarValores(E_FILA e_fila)
        {
            // Inicializar valores
            this.e_fila = new E_FILA();

            // Copiando propiedades
            foreach (var prop in typeof(E_FILA).GetProperties()
                .Where(tipo => tipo.PropertyType == typeof(string)))
            {
                this.e_fila.GetType()
                    .GetProperty(prop.Name).SetValue(
                        this.e_fila, e_fila.GetType().GetProperty(prop.Name).GetValue(e_fila)
                    );
            }

            // Asignando valores del registro
            txt_nombre.Text = e_fila.Nombre;
            nud_stock.Text = e_fila.Stock;
            txt_precio.Text = e_fila.Precio;
            txt_marca.Text = e_fila.Marca;

            n_proceso = new N_PROCESO();
            fueActualizado = false;
            //PROPIEDADES
            btn_actualizar.Enabled = false;
        }
        #region MÉTODOS - FUNCIONES
        private string obtenerRPTA()
        {
            return n_proceso.ACTUALIZAR_REGISTRO(e_fila);
        }
        private void procesarRPTA(string rpta)
        {
            switch (rpta)
            {
                case "ERRORFMTO":
                    MessageBox.Show("Uno de los formatos no es válido", 
                        "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_precio.Clear();
                    break;
                case "ERROREXC":
                    MessageBox.Show("Ocurrió un error inesperado...", "Error", 
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case "HECHO":
                    fueActualizado = true;
                    MessageBox.Show("¡Registro actualizado con éxito!", 
                        "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }
        public bool ObtenerFueActualizado()
        {
            return fueActualizado;
        }
        public void ReiniciarFueActualizado()
        {
            fueActualizado = false;
        }
        #endregion
        #region EVENTOS
        private void btn_actualizar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show(
                "¿Está seguro de los valores que desea actualizar el registro con estos datos datos?", 
                "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (dr.Equals(DialogResult.Yes))
            {
                //RECIBIENDO VALORES
                string nombre = txt_nombre.Text,
                    stock = nud_stock.Text, 
                    precio = txt_precio.Text,
                    marca = txt_marca.Text;

                e_fila.Nombre = nombre;
                e_fila.Stock = stock;
                e_fila.Precio = precio;
                e_fila.Marca = marca;

                string rpta = obtenerRPTA();

                procesarRPTA(rpta);
                fueActualizado = true;
            }
        }
        private void EventoTextChanged_entradaDatos(object sender, EventArgs e)
        {
            if (String.Empty == txt_nombre.Text ||
                String.Empty == txt_precio.Text ||
                String.Empty == txt_marca.Text ||
                nud_stock.Value == 1) btn_actualizar.Enabled = false;
            else btn_actualizar.Enabled = true;
        }
        private void EventoKeyPress_entradaDatos(object sender, KeyPressEventArgs e)
        {
            if (txt_nombre.Focused || txt_marca.Focused)
            {
                if (Char.IsNumber(e.KeyChar) ||
                    Char.IsLetter(e.KeyChar) ||
                    e.KeyChar == '-' ||
                    e.KeyChar == Convert.ToChar(Keys.Back) ||
                    e.KeyChar == Convert.ToChar(Keys.Space)) e.Handled = false;
                else e.Handled = true;
            }
            else if (txt_precio.Focused)
            {
                if (Char.IsNumber(e.KeyChar) || 
                    e.KeyChar == '.' ||
                    e.KeyChar == Convert.ToChar(Keys.Back)) e.Handled = false;
                else e.Handled = true;
            }

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txt_nombre.Focused) nud_stock.Focus();
                else if (nud_stock.Focused) txt_precio.Focus();
                else if (txt_precio.Focused) txt_marca.Focus();
                else if (txt_marca.Focused)
                {
                    btn_actualizar.Focus();
                    btn_actualizar.PerformClick();
                }
            }
        }
        #endregion
    }
}
