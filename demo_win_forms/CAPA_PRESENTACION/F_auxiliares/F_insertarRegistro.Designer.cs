﻿
namespace CAPA_PRESENTACION.F_auxiliares
{
    partial class F_insertarRegistro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_insertar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_nombre = new System.Windows.Forms.TextBox();
            this.cms_bloquearCopiaPega = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.txt_marca = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_precio = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.nud_stock = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.nud_stock)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_insertar
            // 
            this.btn_insertar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_insertar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btn_insertar.Location = new System.Drawing.Point(418, 61);
            this.btn_insertar.Name = "btn_insertar";
            this.btn_insertar.Size = new System.Drawing.Size(94, 38);
            this.btn_insertar.TabIndex = 4;
            this.btn_insertar.Text = "INSERTAR";
            this.btn_insertar.UseVisualStyleBackColor = true;
            this.btn_insertar.Click += new System.EventHandler(this.btn_insertar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "NOMBRE:";
            // 
            // txt_nombre
            // 
            this.txt_nombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_nombre.ContextMenuStrip = this.cms_bloquearCopiaPega;
            this.txt_nombre.Location = new System.Drawing.Point(127, 32);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(224, 20);
            this.txt_nombre.TabIndex = 0;
            this.txt_nombre.TextChanged += new System.EventHandler(this.EventoTextChanged_entradaDatos);
            this.txt_nombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EventoKeyPress_entradaDatos);
            // 
            // cms_bloquearCopiaPega
            // 
            this.cms_bloquearCopiaPega.Name = "cms_bloquearCopiaPega";
            this.cms_bloquearCopiaPega.Size = new System.Drawing.Size(61, 4);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(64, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "STOCK";
            // 
            // txt_marca
            // 
            this.txt_marca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_marca.ContextMenuStrip = this.cms_bloquearCopiaPega;
            this.txt_marca.Location = new System.Drawing.Point(129, 141);
            this.txt_marca.Name = "txt_marca";
            this.txt_marca.Size = new System.Drawing.Size(224, 20);
            this.txt_marca.TabIndex = 3;
            this.txt_marca.TextChanged += new System.EventHandler(this.EventoTextChanged_entradaDatos);
            this.txt_marca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EventoKeyPress_entradaDatos);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "MARCA";
            // 
            // txt_precio
            // 
            this.txt_precio.ContextMenuStrip = this.cms_bloquearCopiaPega;
            this.txt_precio.Location = new System.Drawing.Point(127, 102);
            this.txt_precio.Name = "txt_precio";
            this.txt_precio.Size = new System.Drawing.Size(57, 20);
            this.txt_precio.TabIndex = 2;
            this.txt_precio.TextChanged += new System.EventHandler(this.EventoTextChanged_entradaDatos);
            this.txt_precio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EventoKeyPress_entradaDatos);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(64, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "PRECIO";
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btn_cancelar.Location = new System.Drawing.Point(418, 106);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(94, 38);
            this.btn_cancelar.TabIndex = 5;
            this.btn_cancelar.Text = "CANCELAR";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            // 
            // nud_stock
            // 
            this.nud_stock.ContextMenuStrip = this.cms_bloquearCopiaPega;
            this.nud_stock.Location = new System.Drawing.Point(127, 67);
            this.nud_stock.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            131072});
            this.nud_stock.Name = "nud_stock";
            this.nud_stock.Size = new System.Drawing.Size(57, 20);
            this.nud_stock.TabIndex = 1;
            this.nud_stock.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nud_stock.ValueChanged += new System.EventHandler(this.EventoTextChanged_entradaDatos);
            this.nud_stock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EventoKeyPress_entradaDatos);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_nombre);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.nud_stock);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txt_precio);
            this.groupBox1.Controls.Add(this.txt_marca);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(374, 184);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "RELLENAR";
            // 
            // F_insertarRegistro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(527, 210);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.btn_insertar);
            this.Name = "F_insertarRegistro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "INSERTAR";
            ((System.ComponentModel.ISupportInitialize)(this.nud_stock)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_insertar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_nombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_marca;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_precio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.NumericUpDown nud_stock;
        private System.Windows.Forms.ContextMenuStrip cms_bloquearCopiaPega;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}