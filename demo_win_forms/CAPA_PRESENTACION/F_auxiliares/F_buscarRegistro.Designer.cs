﻿
namespace CAPA_PRESENTACION.F_auxiliares
{
    partial class F_buscarRegistro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_criterio = new System.Windows.Forms.Label();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.txt_criterio = new System.Windows.Forms.TextBox();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_criterio
            // 
            this.lbl_criterio.AutoSize = true;
            this.lbl_criterio.Location = new System.Drawing.Point(40, 28);
            this.lbl_criterio.Name = "lbl_criterio";
            this.lbl_criterio.Size = new System.Drawing.Size(70, 13);
            this.lbl_criterio.TabIndex = 1;
            this.lbl_criterio.Text = "- CRITERIO -";
            // 
            // btn_buscar
            // 
            this.btn_buscar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_buscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btn_buscar.Location = new System.Drawing.Point(208, 17);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(91, 31);
            this.btn_buscar.TabIndex = 2;
            this.btn_buscar.Text = "BUSCAR";
            this.btn_buscar.UseVisualStyleBackColor = true;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // txt_criterio
            // 
            this.txt_criterio.Location = new System.Drawing.Point(31, 44);
            this.txt_criterio.Name = "txt_criterio";
            this.txt_criterio.Size = new System.Drawing.Size(154, 20);
            this.txt_criterio.TabIndex = 0;
            this.txt_criterio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_criterio.TextChanged += new System.EventHandler(this.txt_criterio_TextChanged);
            this.txt_criterio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_criterio_KeyPress);
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btn_cancelar.Location = new System.Drawing.Point(208, 53);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(91, 31);
            this.btn_cancelar.TabIndex = 3;
            this.btn_cancelar.Text = "CANCELAR";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            // 
            // F_buscarRegistro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(326, 98);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.txt_criterio);
            this.Controls.Add(this.btn_buscar);
            this.Controls.Add(this.lbl_criterio);
            this.Name = "F_buscarRegistro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BUSCAR";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_criterio;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.TextBox txt_criterio;
        private System.Windows.Forms.Button btn_cancelar;
    }
}