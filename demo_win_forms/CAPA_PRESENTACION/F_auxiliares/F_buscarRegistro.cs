﻿using SISTEMA_ALMACÉN_LIBRERIA.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CAPA_ENTIDADES;
using CAPA_NEGOCIO;
using CAPA_DATOS;

namespace CAPA_PRESENTACION.F_auxiliares
{
    public partial class F_buscarRegistro : Form
    {
        E_BUSQUEDA e_busqueda;
        N_PROCESO n_proceso;
        TipoBusqueda tipoBusqueda;
        bool fueEncontrado;
        string filaEncontrada;

        public F_buscarRegistro(TipoBusqueda tipoBusqueda)
        {
            InitializeComponent();
            this.tipoBusqueda = tipoBusqueda;
            InicializarValores();
        }
        private void InicializarValores()
        {
            //INICIALIZACIÓN DE VARIABLES Y OBJETOS
            fueEncontrado = true;
            filaEncontrada = "";
            e_busqueda = new E_BUSQUEDA();
            n_proceso = new N_PROCESO();
            //INICIALIZACIÓN DE PROPIEDADES
            switch (tipoBusqueda)
            {
                case TipoBusqueda.Codigo:
                    lbl_criterio.Text = "INGRESE EL CÓDIGO";
                    break;
                case TipoBusqueda.Nombre:
                    lbl_criterio.Text = "INGRESE EL NOMBRE";
                    break;
            }
            btn_buscar.Enabled = false;
            txt_criterio.CharacterCasing = CharacterCasing.Upper;
            txt_criterio.Focus();
        }
        #region MÉTODOS / FUNCIONES
        private void IniciarBusqueda(string criterio)
        {
            e_busqueda.Criterio = criterio;
            if (tipoBusqueda == TipoBusqueda.Nombre) e_busqueda.CodCriterio = "0";
            else if (tipoBusqueda == TipoBusqueda.Codigo) e_busqueda.CodCriterio = "1";

            filaEncontrada = n_proceso.OBTENER_INDICE_BUSQUEDA(e_busqueda);
            fueEncontrado = int.TryParse(filaEncontrada, out int entero);
            /*D_CONSULTAS.msm = filaEncontrada;
            MessageBox.Show(D_CONSULTAS.msm + " asdaskdkas");*/
        }
        public bool ObtenerFueEncontrado()
        {
            return fueEncontrado;
        }
        public void ReiniciarFueEncontrado()
        {
            fueEncontrado = false;
        }
        public string ObtenerFilaEncontrada()
        {
            return filaEncontrada;
        }
        #endregion
        #region EVENTOS
        private void btn_buscar_Click(object sender, EventArgs e)
        {
            string criterio = txt_criterio.Text;
            IniciarBusqueda(criterio);
            txt_criterio.Clear();
        }
        private void txt_criterio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (tipoBusqueda == TipoBusqueda.Codigo)
            {
                if (Char.IsNumber(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)) e.Handled = false;
                else e.Handled = true;
            }
            else if (tipoBusqueda == TipoBusqueda.Nombre)
            {
                if (Char.IsLetterOrDigit(e.KeyChar) ||
                    e.KeyChar == Convert.ToChar(Keys.Space) ||
                    e.KeyChar == Convert.ToChar(Keys.Back)) e.Handled = false;
                else e.Handled = true;
            }
        }
        private void txt_criterio_TextChanged(object sender, EventArgs e)
        {
            btn_buscar.Enabled = !txt_criterio.Text.Equals(String.Empty);
        }
        #endregion
    }
}
