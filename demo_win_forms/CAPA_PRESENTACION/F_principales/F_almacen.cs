﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CAPA_NEGOCIO;
using CAPA_ENTIDADES;
using SISTEMA_ALMACÉN_LIBRERIA.Clases;
using CAPA_PRESENTACION.F_auxiliares;

namespace SISTEMA_ALMACÉN_LIBRERIA
{
    public partial class F_almacen : Form
    {
        #region Atributos
        private N_PROCESO objProceso;
        private DataTable tablaProductos;
        private TipoBusqueda tipoBusqueda;
        private E_FILA focoProducto;
        #endregion

        public F_almacen()
        {
            InitializeComponent();
            InicializarValores();
        }
        private void InicializarValores()
        {
            objProceso = new N_PROCESO();
            tablaProductos = objProceso.OBTENER_TABLA_PRODUCTOS();
            tipoBusqueda = TipoBusqueda.Codigo;
            focoProducto = new E_FILA();

            ManejarHabilitacionBtnActualizarBorrar(false);
        }
        #region MANEJADORES DE EVENTOS
        private void btn_buscarRegistro_Click(object sender, EventArgs e)
        {
            Buscar();
        }
        private void btn_insertarRegistro_Click(object sender, EventArgs e)
        {
            Insertar();
        }
        private void btn_actualizarRegistro_Click(object sender, EventArgs e)
        {
            Actualizar();
        }
        private void btn_borrarRegistro_Click(object sender, EventArgs e)
        {
            Borrar();
        }
        private void rbtn_buscarPor_Click(object sender, EventArgs e)
        {
            if (rbtn_porCod.Focused) tipoBusqueda = TipoBusqueda.Codigo;
            else if (rbtn_porNombre.Focused) tipoBusqueda = TipoBusqueda.Nombre;
        }
        private void F_ALMACEN_Load(object sender, EventArgs e)
        {
            //CARGANDO TABLA DE PRODUCTOS
            dgv_tablaProductos.DataSource = tablaProductos;
        }
        private void dgv_tablaProductos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int filaIdxActual = e.RowIndex;
            filaIdxActual = (filaIdxActual >= 0) ? filaIdxActual : 0;
            FijarFocoRegistroObj(filaIdxActual);
        }

        private void dgv_tablaProductos_Sorted(object sender, EventArgs e) => FijarFocoRegistroObj(0);
        #endregion
        #region MÉTODOS
        private void Buscar()
        {
            F_buscarRegistro f_buscarRegistro = new F_buscarRegistro(tipoBusqueda);
            while (true)
            {
                f_buscarRegistro.ShowDialog(); //INVOCANDO FORMULARIO PARA BÚSQUEDA
                if (f_buscarRegistro.DialogResult == DialogResult.OK)
                {
                    if (f_buscarRegistro.ObtenerFueEncontrado())
                    {
                        string codigo = f_buscarRegistro.ObtenerFilaEncontrada();
                        #region ACTUALIZANDO FOCO
                        for (int i = 0; i < tablaProductos.Rows.Count; i++)
                        {
                            if (tablaProductos.Rows[i]["CÓDIGO"].ToString() == codigo)
                            {
                                FijarFocoRegistroPorIdxFila(i);
                                f_buscarRegistro.ReiniciarFueEncontrado();
                                break;
                            }
                        }
                        #endregion
                        break;
                    }
                    else MessageBox.Show("LO INGRESADO NO FUE ENCONTRADO", "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else break;
            }
        }
        private void Insertar()
        {
            F_insertarRegistro f_insertarRegistro = new F_insertarRegistro();
            while (true)
            {
                f_insertarRegistro.ShowDialog();
                if (f_insertarRegistro.DialogResult == DialogResult.OK)
                {
                    if (f_insertarRegistro.ObtenerFueInsertado())
                    {
                        f_insertarRegistro.ReiniciarFueInsertado();
                        ActualizarTabla();
                        FijarFocoRegistroPorIdxFila(dgv_tablaProductos.RowCount - 2);
                        FijarFocoRegistroObj(dgv_tablaProductos.RowCount - 2);
                        break;
                    }
                }
                else break;
            }
        }
        private void Actualizar()
        {
            F_actualizarRegistro f_ActualizarRegistro = new F_actualizarRegistro(focoProducto);
            while (true)
            {
                f_ActualizarRegistro.ShowDialog();

                if (f_ActualizarRegistro.DialogResult == DialogResult.OK)
                {
                    if (f_ActualizarRegistro.ObtenerFueActualizado())
                    {
                        string codigoPrevio = focoProducto.Codigo;
                        f_ActualizarRegistro.ReiniciarFueActualizado();
                        ActualizarTabla();

                        int filaIdx = dgv_tablaProductos.Rows
                            .Cast<DataGridViewRow>()
                            .ToList()
                            .Find(row => row.Cells[0].Value + "" == codigoPrevio)
                            .Index;
                        FijarFocoRegistroPorIdxFila(filaIdx);
                        FijarFocoRegistroObj(filaIdx);
                        break;
                    }
                }
                else break;
            }
        }
        private void Borrar()
        {
            DialogResult resultado = MessageBox.Show(
                $"¿Está seguro de borrar el producto \"{focoProducto.Nombre}\"?",
                "Borrar registro", 
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (resultado.Equals(DialogResult.OK))
            {
                string rpta = objProceso.ELIMINAR_REGISTRO(new E_BUSQUEDA("1", focoProducto.Codigo));
                switch (rpta)
                {
                    case "ERROR":
                        MessageBox.Show("No se puede eliminar el registro, ocurrió un error inesperado", 
                            "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "ERROREXC":
                        MessageBox.Show("Ocurrió un error inesperado", 
                            "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "NOEXISTE":
                        MessageBox.Show("El registro ingresado no existe", 
                            "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "HECHO":
                        MessageBox.Show(
                            $"¡Borrado con éxito!",
                            "Información",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ActualizarTabla();
                        break;
                }

            }
        }
        private void ActualizarTabla()
        {
            tablaProductos = objProceso.OBTENER_TABLA_PRODUCTOS();
            dgv_tablaProductos.DataSource = tablaProductos;
            dgv_tablaProductos.Sort(dgv_tablaProductos.Columns[0], ListSortDirection.Ascending);
        }
        private void FijarFocoRegistroObj(int filaIdx)
        {
            // Quitando el foco de los registros anteriores
            dgv_tablaProductos.ClearSelection();

            // Obteniendo datos del foco
            focoProducto.Codigo = ObtenerCampoProducto(filaIdx, 0);
            focoProducto.Nombre = ObtenerCampoProducto(filaIdx, 1);
            focoProducto.Stock = ObtenerCampoProducto(filaIdx, 2);
            focoProducto.Precio = ObtenerCampoProducto(filaIdx, 3);
            focoProducto.Marca = ObtenerCampoProducto(filaIdx, 4);

            // Seleccionando registro
            dgv_tablaProductos.Rows[filaIdx].Selected = true;
            ManejarHabilitacionBtnActualizarBorrar(int.Parse(focoProducto.Codigo) >= 1);
        }
        private void FijarFocoRegistroPorIdxFila(int filaIdx)
        {
            dgv_tablaProductos.ClearSelection();
            dgv_tablaProductos.Rows[filaIdx].Selected = true;
            dgv_tablaProductos.FirstDisplayedScrollingRowIndex = filaIdx;
        }
        private string ObtenerCampoProducto(int filaIdx, int celdaIdx)
            => dgv_tablaProductos.Rows[filaIdx].Cells[celdaIdx].Value + "";
        private void ManejarHabilitacionBtnActualizarBorrar(bool value)
        {
            btn_actualizarRegistro.Enabled = value;
            btn_borrarRegistro.Enabled = value;
        }
        #endregion
    }
}
