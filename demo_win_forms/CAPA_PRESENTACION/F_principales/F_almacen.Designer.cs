﻿
namespace SISTEMA_ALMACÉN_LIBRERIA
{
    partial class F_almacen
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_tablaProductos = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_buscarRegistro = new System.Windows.Forms.Button();
            this.gb_busquedaFiltro = new System.Windows.Forms.GroupBox();
            this.rbtn_porNombre = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.rbtn_porCod = new System.Windows.Forms.RadioButton();
            this.btn_insertarRegistro = new System.Windows.Forms.Button();
            this.btn_borrarRegistro = new System.Windows.Forms.Button();
            this.btn_actualizarRegistro = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_tablaProductos)).BeginInit();
            this.gb_busquedaFiltro.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_tablaProductos
            // 
            this.dgv_tablaProductos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.dgv_tablaProductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_tablaProductos.BackgroundColor = System.Drawing.Color.Gray;
            this.dgv_tablaProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_tablaProductos.Location = new System.Drawing.Point(10, 154);
            this.dgv_tablaProductos.MultiSelect = false;
            this.dgv_tablaProductos.Name = "dgv_tablaProductos";
            this.dgv_tablaProductos.ReadOnly = true;
            this.dgv_tablaProductos.Size = new System.Drawing.Size(938, 286);
            this.dgv_tablaProductos.TabIndex = 0;
            this.dgv_tablaProductos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_tablaProductos_CellClick);
            this.dgv_tablaProductos.Sorted += new System.EventHandler(this.dgv_tablaProductos_Sorted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Consolas", 16F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(437, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Inventario";
            // 
            // btn_buscarRegistro
            // 
            this.btn_buscarRegistro.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold);
            this.btn_buscarRegistro.Location = new System.Drawing.Point(316, 20);
            this.btn_buscarRegistro.Name = "btn_buscarRegistro";
            this.btn_buscarRegistro.Size = new System.Drawing.Size(126, 53);
            this.btn_buscarRegistro.TabIndex = 3;
            this.btn_buscarRegistro.Text = "BUSCAR REGISTRO";
            this.btn_buscarRegistro.UseVisualStyleBackColor = true;
            this.btn_buscarRegistro.Click += new System.EventHandler(this.btn_buscarRegistro_Click);
            // 
            // gb_busquedaFiltro
            // 
            this.gb_busquedaFiltro.Controls.Add(this.rbtn_porNombre);
            this.gb_busquedaFiltro.Controls.Add(this.label3);
            this.gb_busquedaFiltro.Controls.Add(this.rbtn_porCod);
            this.gb_busquedaFiltro.Controls.Add(this.btn_buscarRegistro);
            this.gb_busquedaFiltro.Font = new System.Drawing.Font("Consolas", 11F, System.Drawing.FontStyle.Bold);
            this.gb_busquedaFiltro.Location = new System.Drawing.Point(24, 52);
            this.gb_busquedaFiltro.Name = "gb_busquedaFiltro";
            this.gb_busquedaFiltro.Size = new System.Drawing.Size(457, 86);
            this.gb_busquedaFiltro.TabIndex = 4;
            this.gb_busquedaFiltro.TabStop = false;
            this.gb_busquedaFiltro.Text = "BÚSQUEDA";
            // 
            // rbtn_porNombre
            // 
            this.rbtn_porNombre.AutoSize = true;
            this.rbtn_porNombre.Font = new System.Drawing.Font("Consolas", 11F, System.Drawing.FontStyle.Bold);
            this.rbtn_porNombre.Location = new System.Drawing.Point(149, 43);
            this.rbtn_porNombre.Name = "rbtn_porNombre";
            this.rbtn_porNombre.Size = new System.Drawing.Size(74, 22);
            this.rbtn_porNombre.TabIndex = 6;
            this.rbtn_porNombre.Text = "NOMBRE";
            this.rbtn_porNombre.UseVisualStyleBackColor = true;
            this.rbtn_porNombre.Click += new System.EventHandler(this.rbtn_buscarPor_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Consolas", 11F);
            this.label3.Location = new System.Drawing.Point(12, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Por:";
            // 
            // rbtn_porCod
            // 
            this.rbtn_porCod.AutoSize = true;
            this.rbtn_porCod.Checked = true;
            this.rbtn_porCod.Font = new System.Drawing.Font("Consolas", 11F, System.Drawing.FontStyle.Bold);
            this.rbtn_porCod.Location = new System.Drawing.Point(69, 43);
            this.rbtn_porCod.Name = "rbtn_porCod";
            this.rbtn_porCod.Size = new System.Drawing.Size(74, 22);
            this.rbtn_porCod.TabIndex = 4;
            this.rbtn_porCod.TabStop = true;
            this.rbtn_porCod.Text = "CÓDIGO";
            this.rbtn_porCod.UseVisualStyleBackColor = true;
            this.rbtn_porCod.Click += new System.EventHandler(this.rbtn_buscarPor_Click);
            // 
            // btn_insertarRegistro
            // 
            this.btn_insertarRegistro.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold);
            this.btn_insertarRegistro.Location = new System.Drawing.Point(25, 456);
            this.btn_insertarRegistro.Name = "btn_insertarRegistro";
            this.btn_insertarRegistro.Size = new System.Drawing.Size(126, 53);
            this.btn_insertarRegistro.TabIndex = 5;
            this.btn_insertarRegistro.Text = "INSERTAR REGISTRO";
            this.btn_insertarRegistro.UseVisualStyleBackColor = true;
            this.btn_insertarRegistro.Click += new System.EventHandler(this.btn_insertarRegistro_Click);
            // 
            // btn_borrarRegistro
            // 
            this.btn_borrarRegistro.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold);
            this.btn_borrarRegistro.Location = new System.Drawing.Point(819, 456);
            this.btn_borrarRegistro.Name = "btn_borrarRegistro";
            this.btn_borrarRegistro.Size = new System.Drawing.Size(126, 53);
            this.btn_borrarRegistro.TabIndex = 6;
            this.btn_borrarRegistro.Text = "BORRAR REGISTRO";
            this.btn_borrarRegistro.UseVisualStyleBackColor = true;
            this.btn_borrarRegistro.Click += new System.EventHandler(this.btn_borrarRegistro_Click);
            // 
            // btn_actualizarRegistro
            // 
            this.btn_actualizarRegistro.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold);
            this.btn_actualizarRegistro.Location = new System.Drawing.Point(157, 456);
            this.btn_actualizarRegistro.Name = "btn_actualizarRegistro";
            this.btn_actualizarRegistro.Size = new System.Drawing.Size(126, 53);
            this.btn_actualizarRegistro.TabIndex = 7;
            this.btn_actualizarRegistro.Text = "ACTUALIZAR REGISTRO";
            this.btn_actualizarRegistro.UseVisualStyleBackColor = true;
            this.btn_actualizarRegistro.Click += new System.EventHandler(this.btn_actualizarRegistro_Click);
            // 
            // F_almacen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(957, 521);
            this.Controls.Add(this.btn_actualizarRegistro);
            this.Controls.Add(this.btn_borrarRegistro);
            this.Controls.Add(this.btn_insertarRegistro);
            this.Controls.Add(this.gb_busquedaFiltro);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgv_tablaProductos);
            this.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "F_almacen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Almacén Librería";
            this.Load += new System.EventHandler(this.F_ALMACEN_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_tablaProductos)).EndInit();
            this.gb_busquedaFiltro.ResumeLayout(false);
            this.gb_busquedaFiltro.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_tablaProductos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_buscarRegistro;
        private System.Windows.Forms.GroupBox gb_busquedaFiltro;
        private System.Windows.Forms.RadioButton rbtn_porNombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbtn_porCod;
        private System.Windows.Forms.Button btn_insertarRegistro;
        private System.Windows.Forms.Button btn_borrarRegistro;
        private System.Windows.Forms.Button btn_actualizarRegistro;
    }
}

