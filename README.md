# Inventario CRUD Almacén Librería

Aplicación de inventario CRUD para escritorio realizado con las tecnologías: 

* C# con el framework .net WPF
* C# con el framework .net Windows Forms
* SQL Server para la gestion de base de datos

Desarrollado usando una arquitectura en 4 capas: DATOS, ENTIDAD, NEGOCIO y PRESENTACIÓN.


# CRUD Inventory Bookshop Warehouse

Inventory application CRUD for desktop made with the technologies:

* C# with framework .net WPF
* C# with framework .net Windows Forms
* SQL Server for database management

Developed using a 4 layers architecture: DATA, ENITITY, BUSINESS, PRESENTATION.